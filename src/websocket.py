import logging
import os
import sys
import time
import json
from telegram import Bot, ReplyKeyboardRemove
import traceback
from decimal import Decimal

envLambdaTaskRoot = os.environ["LAMBDA_TASK_ROOT"]
print("LAMBDA_TASK_ROOT env var:" + os.environ["LAMBDA_TASK_ROOT"])
print("sys.path:" + str(sys.path))

sys.path.insert(0, envLambdaTaskRoot)
print("sys.path:" + str(sys.path))

import boto3

sys.path.append(os.path.split(__file__)[0])

from helpers import ddb_routines, messages_builder


def _my_default(obj):
    if isinstance(obj, Decimal):
        return float(obj)


# Logging is cool!
logger = logging.getLogger()
if logger.handlers:
    for handler in logger.handlers:
        logger.removeHandler(handler)
logging.basicConfig(level=logging.INFO)


def _get_response(status_code, body):
    if not isinstance(body, str):
        body = json.dumps(body)
    return {"statusCode": status_code, "body": body}


def _get_body(event):
    try:
        return json.loads(event.get("body", ""))
    except:
        logger.debug("event body could not be JSON decoded.")
        return {}


def _send_to_connection_using_event(connection_id, data, event):
    message_string = json.dumps(data, default=_my_default)
    gatewayapi = boto3.client("apigatewaymanagementapi",
                              endpoint_url="https://" + event["requestContext"]["domainName"] +
                              "/" + event["requestContext"]["stage"])
    return gatewayapi.post_to_connection(ConnectionId=connection_id, Data=message_string)


def connectionHandler(event, context):
    logger.info("event: {}".format(event))
    """
    Handles connecting and disconnecting for the Websocket.
    """
    connectionID = event["requestContext"].get("connectionId")

    if event["requestContext"]["eventType"] == "CONNECT":
        logger.info("Connect requested")
        return _get_response(200, "Connect successful.")

    elif event["requestContext"]["eventType"] == "DISCONNECT":
        logger.info("Disconnect requested")
        ddb_routines.delete_connection(connectionID)
        return _get_response(200, "Disconnect successful.")

    else:
        logger.error("Connection manager received unrecognized eventType.")
        return _get_response(500, "Unrecognized eventType.")


def defaultHandler(event, context):
    logger.info("default handler.")
    return _get_response(400, "unknown command")


def getRecordsHandler(event, context):
    body = _get_body(event)
    logger.info('body received: {}'.format(body))
    connectionID = event["requestContext"].get("connectionId")
    records = ddb_routines.get_records_by_time(
        body['data']['startTime'], body['data']['endTime'])
    _send_to_connection_using_event(
        connectionID, {
            'action': 'getRecordsData',
            'data': records
        }, event)
    return _get_response(200, "ok")


def setRealTimeHandler(event, context):
    body = _get_body(event)
    logger.info('body received: {}'.format(body))
    connectionID = event["requestContext"].get("connectionId")
    if body['data']:
        endpoint_url = "https://" + \
            event["requestContext"]["domainName"] + \
            "/" + event["requestContext"]["stage"]
        ddb_routines.create_connection(connectionID, endpoint_url)
    else:
        ddb_routines.delete_connection(connectionID)
    response = {
        'action': 'setRealTime',
        'data': "ok"
    }
    _send_to_connection_using_event(connectionID, response, event)
    return _get_response(200, "ok")
