

import logging
from datetime import datetime
import random


# Logging is cool!
logger = logging.getLogger()
if logger.handlers:
    for handler in logger.handlers:
        logger.removeHandler(handler)
logging.basicConfig(level=logging.INFO)


ask_for_image = ["не забувайте додавати фото, вони дуже корисні",
                 "фото це чи на нейважливіша частина інцидента, не забувайте додавати їх",
                 "якщо у вас є можливіть зробити фото додайте його будь ласка",
                 "просто відправте фото і воно буде додано до інцидента"]

ask_for_more_image = ["чим більше фото буде додано тим краще",
                      "якщо у вас є можливість додати більше ніж одне фото, зробіть це",
                      "одне фото це добре, а декілька ще краще"]

ask_for_more_location = ["дані про локацію чи не найважливіша частина інцидента",
                         "прошу додати локацію якщо є така можливість",
                         "якщо ви хочете додати місце інцидетна просто відправте локацію"]

ask_for_plate_data = ["я ще не вмію розпізнавати номер з фото, прошу додати його окремим полем",
                      "номер авто це дуже важливі дані. команда /add_plate дає можливість це зробити",
                      "за допомогою команди /add_plate можна додати номер авто"]

CONFIRMATION_RESPONSES = ["ок, щось ще?", "прийнято",
                          "збережено, чи є ще інформація?", "записано, якщо є ще щось, прошу надати"]

UNKNOWN_COMMAND = ["я не знаю цієї команди",
                   "хм... спробуйте іншу команду", "нажаль цього я ще не вмію :("]

SERVICE_FAILED = ["вибачте але сталася помилка. спробуйте будь ласка пізніше",
                  "упс... здається у нас проблеми, пробуейте будь ласка пізніше", "хм.. схоже щось зламалось, зверніться будьласка пізніше"]


NO_ACTIVE_SESSION = ["нажаль у вас нема активних заявок. Для створення заявки скористайтесь командою /add",
                     "щоб додати інцидент скористайтесь командою /add", "активні заявки відсутні\nдодати нову - /add\nсписок архівних заявок - /archived"]


def build_ask_for_next_item_text(active_record):
    # logger.info("building message from record    {}".format(active_record))
    missing_items = []
    if "saved_images" not in active_record:
        missing_items.append("saved_images")
    elif len(active_record["saved_images"]) < 2:
        missing_items.append("not_enough_images")
    if "saved_location" not in active_record:
        missing_items.append("saved_location")
    if "plate_data" not in active_record:
        missing_items.append("plate_data")

    logger.info("building message from record    {}".format(missing_items))

    if len(missing_items):
        item_to_ask = random.choice(missing_items)
    else:
        return random.choice(CONFIRMATION_RESPONSES)

    if item_to_ask == "saved_images":
        return random.choice(ask_for_image)
    elif item_to_ask == "not_enough_images":
        return random.choice(ask_for_more_image)
    elif item_to_ask == "saved_location":
        return random.choice(ask_for_more_location)
    elif item_to_ask == "plate_data":
        return random.choice(ask_for_plate_data)


def get_unknown_command_message():
    return random.choice(UNKNOWN_COMMAND)


def get_error_message():
    return random.choice(SERVICE_FAILED)


def get_no_sesion_message():
    return random.choice(NO_ACTIVE_SESSION)


def build_active_message(active_record, final=False):
    logger.info("building message from record    {}".format(active_record))
    message = "Інцидент номер {}:\n".format(active_record['record_number'])
    if "user_comment" in active_record:
        message += "коментарів: {}\n".format(
            len(active_record["user_comment"]))
    if "saved_images" in active_record:
        message += "фото: {}\n".format(len(active_record["saved_images"]))
    if "saved_videos" in active_record:
        message += "відео: {}\n".format(len(active_record["saved_videos"]))
    if "saved_documents" in active_record:
        message += "документи: {}\n".format(
            len(active_record["saved_documents"]))
    if "saved_location" in active_record:
        message += "місцезнаходження додано\n"
    if "plate_data" in active_record:
        message += "номер авто додано\n"
    if not final:
        message += "команда /add_plate щоб додати номер авто окремим полем\n"
        message += "команда /finish щоб завершити і зберегти інцидент\n"
    message += "дякую за небайдужість 🚗  🚓\n"
    return message


def build_archived_message_response(archived_records):
    if len(archived_records):
        message = "список заархівованих заявок:\n"
        now = datetime.now()
        for record in archived_records:
            before = datetime.fromtimestamp(record['createdAt'] / 1000)
            ts = record['sk'].split("|")[1]
            delta = now - before
            seconds = delta.total_seconds()
            hours = seconds // 3600
            minutes = (seconds % 3600) // 60
            delta_string = str(int(hours)) + ":" + \
                str(int(minutes)) + " год:хв назад"
            message += "/activate_{ts} {date}\n".format(
                ts=ts, date=delta_string)
        return message
    else:
        return "у вас нема заархівованих заявок"


def calculate_points_of_record(active_record):
    points = 0
    counter = 0
    if 'plate_data' in active_record.keys():
        points += 1
        counter += 1
    if 'saved_location' in active_record.keys():
        points += 2
        counter += 1
    if 'saved_images' in active_record.keys():
        if len(active_record['saved_images']) >= 3:
            points += 4
        else:
            points += 2
        counter += 1
    if counter >= 3:
        points += 2
    return points
