import boto3
import json
import sys
import os
from decimal import Decimal
import logging
import traceback

from helpers import ddb_routines


# Logging is cool!
logger = logging.getLogger()
if logger.handlers:
    for handler in logger.handlers:
        logger.removeHandler(handler)
logging.basicConfig(level=logging.INFO)

def _my_default(obj):
    if isinstance(obj, Decimal):
        return float(obj)

def _send_to_connection(connection_id, data, endpoint_url):
    message_string = json.dumps(data, default=_my_default)
    gatewayapi = boto3.client(
        "apigatewaymanagementapi", endpoint_url=endpoint_url)
    return gatewayapi.post_to_connection(ConnectionId=connection_id,
                                         Data=message_string)

def sendMessageToAll(message_object):
    connections = ddb_routines.get_all_connections()
    for connection in connections:
        try:
            _send_to_connection(
                connection['sk'], message_object, connection['endpoint_url'])
        except:
            logger.info("error caught")
            error_text = "Unexpected error: {}".format(sys.exc_info())
            tb = traceback.format_exc()
            logger.info(error_text)
            logger.info(tb)


