#
# TODO DDB schematic
# pk -- sk
# User  --  {user_id}
# ActiveRecord -- {user_id}
# FinishedRecord|{user_id} -- Details|{ts}
# UnsupportedData|{user_id} -- Details|{ts}


from boto3.dynamodb.conditions import Key
import boto3
import logging
import os
from decimal import Decimal
import time
import uuid

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])


# Logging is cool!
logger = logging.getLogger()
if logger.handlers:
    for handler in logger.handlers:
        logger.removeHandler(handler)
logging.basicConfig(level=logging.INFO)


def set_last_update_time(user_id, ts=int(time.time() * 1000)):
    table.update_item(
        Key={
            'pk': "User",
            'sk': str(user_id)
        },
        UpdateExpression='SET lastEventTime = :timestamp',
        ExpressionAttributeValues={
            ':timestamp': ts
        }
    )


def count_finished_records(user_id):
    logger.info("count records of user {id}".format(id=user_id))

    response = table.query(
        Select='COUNT',
        KeyConditionExpression=Key('pk').eq("FinishedRecord|" + str(user_id)))
    logger.info("count response {}".format(response))

    return response['Count']

# records manipulations
###################################


def finish_active_record(active_record):
    delete_active_record(active_record)
    return create_finished_record(active_record)


def archive_active_record(active_record):
    delete_active_record(active_record)
    create_archived_record(active_record)

# creates trash can record in db and removes active record


def cancel_active_recrod(active_record):
    delete_active_record(active_record)
    create_canceled_record(active_record)


def activate_archived_record(archived_record):
    delete_archived_record(archived_record)
    active_record = create_active_record_from_archived(archived_record)
    return active_record

# active record routines
###################################


def create_active_record(update):
    timestamp = int(time.time() * 1000)

    user_records_count = get_user(update.message.from_user.id)
    if not user_records_count:
        user_records_count = create_user(
            update.message.from_user.id, update)
    logger.info('found user count {}'.format(user_records_count))
    update_incident_counter_count_record(
        user_records_count, user_records_count['records_count'] + 1)

    global_records_count = get_incident_counter()
    if not global_records_count:
        global_records_count = create_incident_counter_record()
    logger.info('found global count {}'.format(global_records_count))
    update_incident_counter_count_record(
        global_records_count, global_records_count['records_count'] + 1)

    item = {
        'pk': "ActiveRecord",
        'sk': str(update.message.from_user['id']),
        # 'body': update,
        'createdAt': timestamp,
        'updatedAt': timestamp,
        'record_number': user_records_count['records_count'] + 1,
        'global_number': global_records_count['records_count'] + 1
    }
    table.put_item(Item=item)
    return item


def create_active_record_from_archived(archived_record):
    timestamp = int(time.time() * 1000)
    user_id = archived_record['pk'].split("|")[1]

    archived_record.pop('pk', None)
    archived_record.pop('sk', None)

    archived_record['updatedAt'] = timestamp

    item = {
        'pk': "ActiveRecord",
        'sk': str(user_id),
        # 'body': update,
        **archived_record
    }

    table.put_item(Item=item)
    return item


def delete_active_record(active_record):
    logger.info("removing active record {}".format(active_record['sk']))
    result = table.delete_item(
        Key={
            'pk': active_record['pk'],
            'sk': active_record['sk']
        }
    )
    logger.info("removing active record result {}".format(result))


def get_active_record(user_id):
    logger.info("finding actuve record {id}".format(
        id=user_id))
    response = table.get_item(
        Key={
            'pk': "ActiveRecord",
            'sk': str(user_id)
        }
    )

    logger.info('found record: {}'.format(response))
    return response.get("Item", None)


def find_active_records():
    logger.info("find all active records")
    response = table.query(
        KeyConditionExpression=Key('pk').eq("ActiveRecord"))

    logger.info("count response {}".format(response))

    return response.get("Items", None)


def add_message_id(item, message_id):
    if 'message_id' not in item.keys():
        logger.info('creating message_id')
        item['message_id'] = message_id

    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET message_id = :message_id, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':message_id': item['message_id'],
            ':timestamp': ts
        }
    )


def add_comment_to_item(item, user_comment):
    if 'user_comment' not in item.keys():
        logger.info('creating comment field')
        item['user_comment'] = []

    item['user_comment'].append(user_comment)

    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET user_comment = :user_comment, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':user_comment': item['user_comment'],
            ':timestamp': ts
        }
    )


def add_location_to_item(item, location):
    location_present = False
    if 'saved_location' in item.keys():
        logger.info("location is already present")
        location_present = True
    item["saved_location"] = {
        'latitude': Decimal(str(location.latitude)),
        'longitude': Decimal(str(location.longitude))
    }

    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET saved_location = :saved_location, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':saved_location': item['saved_location'],
            ':timestamp': ts
        }
    )
    return location_present


def get_largest_image(image):
    largest_item = image[0].file_id
    largest_value = image[0].file_size
    for item in image:
        if item.file_size > largest_value:
            largest_value = item.file_size
            largest_item = item.file_id
    logger.info("largest item is {}".format(largest_item))
    return largest_item


def add_image_to_item(item, image_id):
    if 'saved_images' not in item.keys():
        logger.info("saved_images are not present")
        item['saved_images'] = []

    item['saved_images'].append(image_id)
    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET saved_images = :saved_images, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':saved_images': item['saved_images'],
            ':timestamp': ts
        }
    )


def add_document_to_item(item, document):
    if 'saved_documents' not in item.keys():
        logger.info("saved_images are not present")
        item['saved_documents'] = []

    item['saved_documents'].append({
        'file_id': document.file_id,
        'file_name': document.file_name,
        'mime_type': document.mime_type
    })
    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET saved_documents = :saved_documents, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':saved_documents': item['saved_documents'],
            ':timestamp': ts
        }
    )


def add_video_to_item(item, video):
    if 'saved_videos' not in item.keys():
        logger.info("saved_images are not present")
        item['saved_videos'] = []

    item['saved_videos'].append({
        'file_id': video.file_id,
        'mime_type': video.mime_type
    })
    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET saved_videos = :saved_videos, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':saved_videos': item['saved_videos'],
            ':timestamp': ts
        }
    )


def add_plate_adding_flag(item):
    item['add_plate_flag'] = True

    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET add_plate_flag = :add_plate_flag, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':add_plate_flag': item['add_plate_flag'],
            ':timestamp': ts
        }
    )


def add_plate_to_item(item, plate_data):

    item['plate_data'] = plate_data

    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET add_plate_flag = :add_plate_flag, plate_data = :plate_data, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':add_plate_flag': False,
            ':plate_data': item['plate_data'],
            ':timestamp': ts
        }
    )

# finished record routines
###################################


def create_finished_record(active_record):
    timestamp = int(time.time() * 1000)
    user_id = active_record['sk']

    active_record.pop('pk', None)
    active_record.pop('sk', None)

    item = {
        'pk': "FinishedRecord|" + str(user_id),
        'sk': "Details|" + str(active_record['createdAt']),
        "user_id": str(user_id),
        'id': str(uuid.uuid4()),
        'finishedAt': timestamp,
        'global_order_pk': 'FinishedRecord',
        'global_order_sk': timestamp,
        **active_record
    }
    table.put_item(Item=item)
    return item

# deleted record routines
###################################


def create_canceled_record(active_record):
    timestamp = int(time.time() * 1000)
    user_id = active_record['sk']

    active_record.pop('pk', None)
    active_record.pop('sk', None)

    item = {
        'pk': "CanceledRecord|" + str(user_id),
        'sk': "Details|" + str(active_record['createdAt']),
        'canceledAt': timestamp,
        'global_order_pk': 'CanceledRecord',
        'global_order_sk': timestamp,
        **active_record
    }
    table.put_item(Item=item)


# archived record routines
###################################
def create_archived_record(active_record):
    logger.info("archiving record {}".format(active_record['sk']))
    timestamp = int(time.time() * 1000)
    user_id = active_record['sk']

    active_record.pop('pk', None)
    active_record.pop('sk', None)

    item = {
        'pk': "ArchivedRecord|" + str(user_id),
        'sk': "Details|" + str(active_record['createdAt']),
        'archivedAt': timestamp,
        **active_record
    }
    table.put_item(Item=item)


def find_archived_records(user_id):
    logger.info("find all archived records")
    response = table.query(
        KeyConditionExpression=Key('pk').eq("ArchivedRecord|" + str(user_id))
        & Key('sk').begins_with("Details|"))

    logger.info("archived response {}".format(response))

    return response.get("Items", None)


def get_archived_record(user_id, ts):
    logger.info("finding archived record for user: {id} ts: {ts}".format(
        id=user_id, ts=ts))
    response = table.get_item(
        Key={
            'pk': "ArchivedRecord|" + str(user_id),
            'sk': 'Details|' + str(ts)
        }
    )

    logger.info('found archived record: {}'.format(response))
    return response.get('Item', None)


def delete_archived_record(archved_record):
    logger.info("removing archived record for user: {}".format(
        archved_record['pk']))
    response = table.delete_item(
        Key={
            'pk': archved_record['pk'],
            'sk': archved_record['sk']
        }
    )

    logger.info('removed archived record: {}'.format(response))

# UnsupportedData record
######################################


def create_unsupported_data_record(user_id, data, data_type):
    timestamp = int(time.time() * 1000)
    item = {
        'pk': "UnsupportedData|" + str(user_id),
        'sk': "Details|" + str(timestamp),
        # 'body': update,
        'createdAt': timestamp,
        'updatedAt': timestamp,
        'data': data,
        'data_type': data_type,
        'global_order_pk': 'UnsupportedData',
        'global_order_sk': timestamp
    }
    table.put_item(Item=item)
    return item

# global incident counter record
###############################


def create_incident_counter_record():
    timestamp = int(time.time() * 1000)

    item = {
        'pk': "GlobalIncidentCounter",
        'sk': "Details",
        'createdAt': timestamp,
        'updatedAt': timestamp,
        'records_count': 0
    }
    table.put_item(Item=item)
    return item


def update_incident_counter_count_record(item, count):
    ts = int(time.time() * 1000)
    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET records_count = :records_count, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':records_count': count,
            ':timestamp': ts
        }
    )


def get_incident_counter():
    response = table.get_item(
        Key={
            'pk': "GlobalIncidentCounter",
            'sk': 'Details'
        }
    )

    logger.info('found incident counter record: {}'.format(response))
    return response.get('Item', None)

# user records
###############################


def create_user(user_id, update):
    timestamp = int(time.time() * 1000)

    # TODO add recursive saving of from_user field content
    item = {
        'pk': "User",
        'sk': str(user_id),
        'createdAt': timestamp,
        'updatedAt': timestamp,
        'records_count': 0,
        'data': {
            "id": update.message.from_user['id'],
            "first_name": update.message.from_user['first_name'],
            "username": update.message.from_user['username'],
            "language_code": update.message.from_user['language_code']
        },
    }
    table.put_item(Item=item)
    return item


def get_user(user_id):
    response = table.get_item(
        Key={
            'pk': "User",
            'sk': str(user_id)
        }
    )

    logger.info('found incident counter record: {}'.format(response))
    return response.get('Item', None)


def add_points_to_item(item, points):
    ts = int(time.time() * 1000)

    table.update_item(
        Key={
            'pk': item['pk'],
            'sk': item['sk']
        },
        UpdateExpression='SET points = :points, updatedAt = :timestamp',
        ExpressionAttributeValues={
            ':points': points + item.get("points", 0),
            ':timestamp': ts
        }
    )

# creates record about error in code


def create_error_report(error_data, update):
    timestamp = int(time.time() * 1000)
    item = {
        'pk': "ErrorReport",
        'sk': str(uuid.uuid4()),
        "error_data": error_data,
        'global_order_pk': 'ErrorReport',
        'global_order_sk': timestamp,
    }
    if update:
        item['user_id'] = update.message.from_user['id']

    table.put_item(Item=item)
    return item


# websoket connections
# 33
def create_connection(connection_id, endpoint_url):
    item = {
        'pk': "WebsocketConnection",
        'sk': str(connection_id),
        'endpoint_url': endpoint_url
    }
    table.put_item(Item=item)
    return item


def delete_connection(connection_id):
    table.delete_item(
        Key={
            'pk': "WebsocketConnection",
            'sk': str(connection_id),
        }
    )


def get_all_connections():
    logger.info("find all connections")
    response = table.query(
        KeyConditionExpression=Key('pk').eq("WebsocketConnection"))

    return response.get("Items", None)


def get_connection(connection_id):
    response = table.get_item(
        Key={
            'pk': "WebsocketConnection",
            'sk': str(connection_id)
        }
    )
    return response.get("Item", None)


def get_records_by_time(startTime, endTime):
    response = table.query(
        IndexName='global-order-index',
        ProjectionExpression='id,saved_location',
        KeyConditionExpression=Key('global_order_pk').eq("FinishedRecord") &
        Key('global_order_sk').between(startTime-1, endTime+1))
    return response.get("Items", None)
