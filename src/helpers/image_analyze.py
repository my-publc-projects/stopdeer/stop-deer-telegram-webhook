#!/bin/python

import boto3
from PIL import Image
from io import BytesIO
from PIL.ExifTags import TAGS

size_limit = 5000000

def is_car_on_photo(image_bytes):
    client = boto3.client('rekognition')
    resp_obj = {}
    img = Image.open(BytesIO(image_bytes))
    if img._getexif():
        exif = { TAGS[k]: v for k, v in img._getexif().items() if k in TAGS }
        resp_obj["exif"] = exif
        print('exif data is: {}'.format(exif))
    else: 
        print('there are no exif data')
        resp_obj["exif"] = None

    if len(image_bytes) > size_limit:
        print("need to resize image {}".format(len(image_bytes)))
        # percents = (size_limit / len(image_bytes))
        # percents += (1.0 - percents) / 2
        # img = img.resize(
        #     (int(img.width * percents), int(img.height * percents)), Image.ANTIALIAS)
        with BytesIO() as output:
            img.save(output, format="JPEG")
            image_bytes = output.getvalue()
        print("resized image size is {}".format(len(image_bytes)))

    response = client.detect_labels(Image={'Bytes': image_bytes})

    print('Detected labels are: ')
    for label in response['Labels']:
        print(label['Name'] + ' : ' + str(label['Confidence']))

    resp_obj["labels"] = response['Labels']

    for label in response['Labels']:
        if label["Name"] in ["Car", "Vehicle", "Bus", "Automobile"] and label['Confidence'] > 70:
            resp_obj["car_present"] = True
            return resp_obj

    resp_obj["car_present"] = False
    return resp_obj
