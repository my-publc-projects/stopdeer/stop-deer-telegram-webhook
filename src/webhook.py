from telegram import Bot, Update, InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

from telegram.ext import CommandHandler
import json
import sys
import uuid
import os
import logging
import traceback

# this makes local modules work
sys.path.append(os.path.split(__file__)[0])

from helpers import messages_builder, ddb_routines, image_analyze, websocket


# Logging is cool!
logger = logging.getLogger()
if logger.handlers:
    for handler in logger.handlers:
        logger.removeHandler(handler)
logging.basicConfig(level=logging.INFO)

OK_RESPONSE = {
    'statusCode': 200,
    'headers': {'Content-Type': 'application/json'},
    'body': json.dumps('ok')
}

ERROR_RESPONSE = {
    'statusCode': 400,
    'body': json.dumps('Oops, something went wrong!')
}

# configure telegram with token, returns bot instance


def create_incident(bot, update):
    active_record = ddb_routines.create_active_record(
        update)

    result = bot.sendMessage(
        chat_id=update.message.chat.id,
        text=messages_builder.build_active_message(
            active_record))

    ddb_routines.add_message_id(
        active_record, result.message_id)

    return active_record


def configure_telegram():
    TELEGRAM_TOKEN = os.environ.get('TELEGRAM_TOKEN')
    if not TELEGRAM_TOKEN:
        logger.error('The TELEGRAM_TOKEN must be set')
        raise NotImplementedError

    return Bot(TELEGRAM_TOKEN)

# updates message mapped to incident


def update_message(bot, active_record, chat_id):
    try:
        bot.edit_message_text(chat_id=chat_id,
                              message_id=int(active_record['message_id']), text=messages_builder. build_active_message(active_record))
        return True
    except:
        logger.info("failed to update message")
        logger.info("Unexpected error: {}".format(sys.exc_info()))
        traceback.print_exc()
        return False

# builds keyboard with buttons


def build_keyboard_markup():
    location_keyboard = KeyboardButton(text="/add_plate")
    contact_keyboard = KeyboardButton(text="/finish")
    cancel_keyboard = KeyboardButton(text="/cancel")
    custom_keyboard = [[location_keyboard, contact_keyboard, cancel_keyboard]]
    reply_markup = ReplyKeyboardMarkup(
        custom_keyboard, one_time_keyboard=False, resize_keyboard=True)
    return reply_markup

# builds message after another data arriving


def continue_dialog(bot, update, active_record):
    bot.sendMessage(chat_id=update.message.chat.id, text=messages_builder.build_ask_for_next_item_text(
        active_record), reply_markup=build_keyboard_markup())

# main methid of webhook


def handle(event, context):
    bot = configure_telegram()
    logger.info(os.path.split(__file__)[0])
    logger.info('Event: {}'.format(event.get('body')))

    if event.get('httpMethod') == 'POST' and event.get('body'):
        # logger.info('Message received {}'.format(event.get('body')))
        update = Update.de_json(json.loads(
            event.get('body')), bot)
        logger.info('update parsed {}'.format(update))

        if update.message:
            chat_id = update.message.chat.id
            message = update.message.text

            try:
                if update.message.from_user.is_bot:
                    bot.sendMessage(chat_id=chat_id,
                                    text="вибачте але мені здається що ви бот")
                    return OK_RESPONSE

                # check is the record present
                active_record = ddb_routines.get_active_record(
                    update.message.from_user['id'])

                # handle text message
                if update.message.text:
                    logger.info('handle text message {}'.format(
                        update.message.text))
                    if message == '/start':
                        if ddb_routines.get_user(update.message.from_user.id) == None:
                            ddb_routines.create_user(
                                update.message.from_user.id, update)
                        text = "Вітаю вас. цей бот дає можливість надавати інформацію про авто припарковані з порушенням. Ви можете додати фото порушення, локацію, коментарі та номер авто. щоб почати скористайтесь командою /add або просто почніть з відправки фото або локації.\nДля допомоги скористайтесь командою /help"
                        bot.sendMessage(chat_id=chat_id, text=text)
                    elif message == '/help':
                        text = "Цей бот створено щоб дати користувачам простий інструмент додавання правопорушеннь пов'язаних з неправильним паркуванням. Фото та локація є обов'язковими складовими. Щоб створені вами заявки несли максимально повний опис інцидента прошу надавати максимально можливу кількість інформації.\n/add - додати заявку\n/finish - зберегти активну заявку\n/cancel - відмінити активну заявку\n/add_plate - додати номер авто до активної заявки\n/stats - отримати статистику користування ботом\n/archived - отримати список заархівованих заявок"
                        bot.sendMessage(chat_id=chat_id, text=text)
                    elif message == '/finish':
                        if active_record:
                            user_record = ddb_routines.get_user(
                                update.message.from_user.id)
                            points = messages_builder.calculate_points_of_record(
                                active_record)
                            logger.info("record points: {}".format(points))
                            if active_record.get("saved_location", None) and active_record.get("saved_images", None):
                                ddb_routines.add_points_to_item(
                                    user_record, points)

                                text = "ваша заявка збережена\n" + \
                                    messages_builder.build_active_message(
                                        active_record, True) +\
                                    "\nви можете додати нову командою /add"
                                finsished_item = ddb_routines.finish_active_record(
                                    active_record)
                                p = ReplyKeyboardRemove()
                                bot.sendMessage(chat_id=chat_id,
                                                text=text, reply_markup=p)
                                websocket.sendMessageToAll({
                                    'action': 'realTimeData',
                                    'data': {
                                        'id': finsished_item['id'],
                                        'saved_location': finsished_item.get('saved_location', None)
                                    }
                                })
                            elif not active_record.get("saved_location", None):
                                bot.sendMessage(chat_id=chat_id,
                                                text="неможливо зберегти інцидент без данних локації, його можна відмінити командою /cancel або додати необхідні дані")
                            else:
                                bot.sendMessage(chat_id=chat_id,
                                                text="неможливо зберегти інцидент без фото, його можна відмінити командою /cancel або додати необхідні дані")
                        else:
                            bot.sendMessage(chat_id=chat_id,
                                            text=messages_builder.get_no_sesion_message())
                    elif message == '/cancel':
                        if active_record:
                            text = "ваша заявка відмінена\n" + \
                                messages_builder.build_active_message(
                                    active_record, True) +\
                                "\nви можете додати нову командою /add"
                            ddb_routines.cancel_active_recrod(active_record)
                            p = ReplyKeyboardRemove()
                            bot.sendMessage(chat_id=chat_id,
                                            text=text, reply_markup=p)
                        else:
                            bot.sendMessage(chat_id=chat_id,
                                            text=messages_builder.get_no_sesion_message())
                    elif message == '/archived':
                        archved_records = ddb_routines.find_archived_records(
                            update.message.from_user.id)
                        p = ReplyKeyboardRemove()
                        bot.sendMessage(chat_id=chat_id,
                                        text=messages_builder.build_archived_message_response(archved_records), reply_markup=p)
                    elif message == '/stats':
                        user_record = ddb_routines.get_user(
                            update.message.from_user.id)
                        finished_records = ddb_routines.count_finished_records(
                            update.message.from_user.id)
                        p = ReplyKeyboardRemove()
                        bot.sendMessage(chat_id=chat_id,
                                        text="на вашому рахунку {i} інцидентів загальною вартістю в {j} балів".format(i=finished_records, j=user_record['points']), reply_markup=p)

                    elif message.startswith("/activate"):
                        was_active = False
                        if active_record:
                            was_active = True
                            ddb_routines.archive_active_record(active_record)
                        ts = message.split('_')[1]
                        archived_record = ddb_routines.get_archived_record(
                            update.message.from_user.id, ts)
                        if archived_record:
                            active_record = ddb_routines.activate_archived_record(
                                archived_record)
                            p = ReplyKeyboardRemove()
                            reply = "архівований інцидент активовано\n" + messages_builder.build_active_message(
                                active_record, True) + "\nви можете пордовжити додавати до нього дані"
                            if was_active:
                                reply += "\nваша попередня активна заявка архівована"
                            bot.sendMessage(chat_id=chat_id,
                                            text=reply, reply_markup=p)
                        else:
                            p = ReplyKeyboardRemove()
                            bot.sendMessage(chat_id=chat_id,
                                            text="архівований інцидент не знайдено", reply_markup=p)

                    elif message == '/add':
                        # mark active record as finished if it present
                        if active_record:
                            ddb_routines.archive_active_record(active_record)
                        create_incident(bot, update)

                    elif message == '/add_plate':
                        if active_record:
                            ddb_routines.add_plate_adding_flag(active_record)
                            bot.sendMessage(chat_id=chat_id,
                                            text="введіть будь ласка номер авто")
                        else:
                            bot.sendMessage(chat_id=chat_id,
                                            text=messages_builder.get_no_sesion_message())
                    elif message.startswith("/"):
                        bot.sendMessage(chat_id=chat_id,
                                        text=messages_builder.get_unknown_command_message())
                    elif active_record:
                        if "add_plate_flag" in active_record.keys() and active_record["add_plate_flag"] == True:
                            ddb_routines.add_plate_to_item(
                                active_record, message)
                            continue_dialog(bot, update, active_record)
                        else:
                            ddb_routines.add_comment_to_item(
                                active_record, message)
                            continue_dialog(bot, update, active_record)
                        # logger.info(' active_record {}'.format(active_record))
                        update_message(bot, active_record,
                                       update.message.chat.id)
                    else:
                        bot.sendMessage(chat_id=chat_id,
                                        text=messages_builder.get_no_sesion_message())

                # handle location
                elif update.message.location:
                    logger.info('handle location message {}'.format(
                        update.message.location))

                    if not active_record:
                        active_record = create_incident(bot, update)

                    update_message(bot, active_record,
                                   update.message.chat.id)
                    ddb_routines.add_location_to_item(
                        active_record, update.message.location)
                    continue_dialog(bot, update, active_record)

                # handle photo
                elif update.message.photo:
                    logger.info('handle photo message {}'.format(
                        update.message.photo))

                    largest_image = ddb_routines.get_largest_image(
                        update.message.photo)
                    photo_file = bot.get_file(largest_image)
                    analyze_response = image_analyze.is_car_on_photo(
                        photo_file.download_as_bytearray())

                    if analyze_response["car_present"]:
                        if not active_record:
                            active_record = create_incident(bot, update)
                        ddb_routines.add_image_to_item(
                            active_record, largest_image)
                        update_message(bot, active_record,
                                        update.message.chat.id)
                        continue_dialog(bot, update, active_record)
                    else:
                        obj = {
                            'file_id': largest_image
                        }
                        ddb_routines.create_unsupported_data_record(
                            update.message.from_user.id, obj, "unrecognized_photo")
                        bot.sendMessage(
                            chat_id=chat_id, text="Здається на цьому фото немає автомобіля, можливо фото не дуже детальне.")
                    

                # handle file
                elif update.message.document:
                    logger.info('handle file message {}'.format(
                        update.message.document))
                    if active_record:
                        if (update.message.document.mime_type and update.message.document.mime_type.startswith('image/') or (update.message.document.file_name and update.message.document.file_name.endswith('.png'))):
                            photo_file = bot.get_file(
                                update.message.document.file_id)
                            analyze_response = image_analyze.is_car_on_photo(
                                photo_file.download_as_bytearray())
                            if analyze_response["car_present"]:
                                ddb_routines.add_image_to_item(
                                    active_record, update.message.document.file_id)
                                update_message(
                                    bot, active_record, update.message.chat.id)
                                continue_dialog(bot, update, active_record)
                            else:
                                obj = {
                                    'file_id': update.message.document.file_id
                                }
                                ddb_routines.create_unsupported_data_record(
                                    update.message.from_user.id, obj, "unrecognized_photo")
                                bot.sendMessage(
                                    chat_id=chat_id, text="Здається на цьому фото немає автомобіля, можливо фото не дуже детальне.")
                        else:
                            bot.send_message(
                                chat_id=chat_id, text="нажаль дані цього типу не підтримується.")
                            obj = {
                                'file_id': update.message.document.file_id
                            }
                            ddb_routines.create_unsupported_data_record(
                                update.message.from_user.id, obj, "document")
                    else:
                        bot.sendMessage(chat_id=chat_id,
                                        text=messages_builder.get_no_sesion_message())
                else:
                    bot.send_message(
                        chat_id=chat_id, text="нажаль дані цього типу не підтримується.")

                    obj = {}
                    obj_type = ""
                    if update.message.sticker:
                        # obj = update.message.sticker
                        obj_type = "sticker"
                    elif update.message.video:
                        obj = {
                            'file_id': update.message.video.file_id
                        }
                        obj_type = "video"
                    elif update.message.voice:
                        obj = {
                            'file_id': update.message.voice.file_id
                        }
                        obj_type = "voice"
                    elif update.message.game:
                        obj = {}
                        obj_type = "game"
                    elif update.message.animation:
                        obj = {
                            'file_id': update.message.animation.file_id
                        }
                        obj_type = "animation"
                    elif update.message.contact:
                        obj = {
                            'phone_number': update.message.contact.phone_number,
                            'first_name': update.message.contact.first_name,
                            'last_name': update.message.contact.last_name,
                            'user_id': update.message.contact.user_id
                        }
                        obj_type = "contact"
                    elif update.message.venue:
                        obj = {
                            'title': update.message.contact.title,
                            'address': update.message.contact.address,
                            'foursquare_id': update.message.contact.foursquare_id,
                            'foursquare_type': update.message.contact.foursquare_type
                        }
                        obj_type = "venue"
                    elif update.message.invoice:
                        obj = {
                            'title': update.message.contact.title,
                            'description': update.message.contact.description,
                            'start_parameter': update.message.contact.start_parameter,
                            'currency': update.message.contact.currency,
                            'total_amount': update.message.contact.total_amount
                        }
                        obj_type = "invoice"
                    else:
                        obj = {
                        }
                        obj_type = "other"

                    # TODO make object save process active_record.pop('pk', None)
                    ddb_routines.create_unsupported_data_record(
                        update.message.from_user.id, obj, obj_type)

                return OK_RESPONSE

            except:
                logger.info("error caught")
                error_text = "Unexpected error: {}".format(sys.exc_info())
                tb = traceback.format_exc()
                logger.info(error_text)
                logger.info(tb)
                ddb_routines.create_error_report(
                    {"data": error_text, "tb": tb}, update)
                bot.sendMessage(chat_id=chat_id,
                                text=messages_builder.get_error_message())
                return OK_RESPONSE

    return ERROR_RESPONSE
