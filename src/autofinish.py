import logging
import os
import sys
import time
from telegram import Bot, ReplyKeyboardRemove
import traceback

sys.path.append(os.path.split(__file__)[0])

from helpers import ddb_routines, messages_builder

# Logging is cool!
logger = logging.getLogger()
if logger.handlers:
    for handler in logger.handlers:
        logger.removeHandler(handler)
logging.basicConfig(level=logging.INFO)

# todo implement points calculation


def handle(event, context):
    try:    
        not_finished = ddb_routines.find_active_records()
        logger.info('autofinish Event: {}'.format(event))
        logger.info('autofinish: {}'.format(len(not_finished)))

        if len(not_finished):
            """
            Configures the bot with a Telegram Token.
            Returns a bot instance.
            """

            TELEGRAM_TOKEN = os.environ.get('TELEGRAM_TOKEN')
            if not TELEGRAM_TOKEN:
                logger.error('The TELEGRAM_TOKEN must be set')
                raise NotImplementedError

            bot = Bot(TELEGRAM_TOKEN)

            record_ttl = 10 * 60 * 1000  # ms
            timestamp = int(time.time() * 1000) - record_ttl

            logger.info('time rule: {}'.format(timestamp))

            for active_record in not_finished:
                if active_record['updatedAt'] < timestamp:
                    logger.info("should remove record {}".format(active_record))
                    user_id = active_record['sk']
                    ddb_routines.archive_active_record(active_record)

                    text = "ваша заявка заархівована так як не була збережена\n" + \
                        messages_builder.build_active_message(
                            active_record, True) +\
                        "\nактивувати архівовану заявку можна вибравши її зі списку архівованих, щоб отримати список архівованих скористайтесь командою /archived"
                    p = ReplyKeyboardRemove()
                    bot.sendMessage(chat_id=user_id,text=text, reply_markup=p)
    except:
        logger.info("error caught")
        error_text = "Unexpected error: {}".format(sys.exc_info())
        tb = traceback.format_exc()
        logger.info(error_text)
        logger.info(tb)
        ddb_routines.create_error_report(
            {"data": error_text, "tb": tb}, None)
